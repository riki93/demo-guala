package com.example.demo.rest;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.ToLongFunction;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Ntf;
import com.example.demo.repository.NtfRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/ntf")
public class NtfController {
 
    @Autowired
    private NtfRepository testRepository;
    
    private Comparator<Ntf> sort=new Comparator<Ntf>() {
		@Override
		public int compare(Ntf o1, Ntf o2) {
			return o1.getName().compareTo(o2.getName());
		}
	};
        
    @GetMapping("findAll")
    public <T> List<Ntf> findAllUsers() {
      List<Ntf> all=(List<Ntf>) testRepository.findAll();
      all.sort(sort);
      return all;
    }
 
    @GetMapping("/{id}")
    public ResponseEntity<Ntf> findUserById(@PathVariable(value = "id") long id) {
    	Optional<Ntf> ntf=testRepository.findById(id);
		if(ntf.isPresent()) {
			 return ResponseEntity.ok().body(ntf.get());
		}else {
			 return ResponseEntity.notFound().build();
		}
    }
 
  
}
