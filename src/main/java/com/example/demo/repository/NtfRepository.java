package com.example.demo.repository;
import com.example.demo.entity.Ntf;



import org.springframework.data.repository.CrudRepository;

public interface NtfRepository extends CrudRepository<Ntf, Long> {

}
