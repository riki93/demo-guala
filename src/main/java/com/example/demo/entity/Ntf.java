package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ntf")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Ntf {
    
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private String volume ;
    private String volume_USD ;
    private String market_Cap ;
    private String market_Cap_USD ;
    private String sales ;
    private String floor_Price ;
    private String floor_Price_USD ;
    private String average_Price ;
    private String average_Price_USD ;
    private String owners,Assets ;
    private String owner_Asset_Ratio ;
    private String category ;
    private String website ;
    private String logo  ;
    
    
    
    
    
}
